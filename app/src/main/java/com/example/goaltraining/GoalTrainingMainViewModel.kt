package com.example.goaltraining

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Build
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.goaltraining.ui.screens.Screens
import com.example.goaltraining.util.*
import com.onesignal.OneSignal
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class GoalTrainingMainViewModel: ViewModel() {
    private var request: Job? = null
    private var backDownload: Job? = null
    private val assetTargets = ArrayList<Target>()
    private var assetsToDownload = 0

    // Public

    fun init(service: String, id: String) {
        request = viewModelScope.async {
            requestSplash(service, id)
        }
    }

    fun retryDownload() {
        Navigator.navigateTo(Screens.SPLASH_SCREEN)
        downloadAssets()
    }

    /// Private

    private suspend fun requestSplash(service: String, id: String) {
        try {
            val time = SimpleDateFormat("z", Locale.getDefault()).format(
                Calendar.getInstance(
                    TimeZone.getTimeZone("GMT"), Locale.getDefault()
                ).time
            )
                .replace("GMT", "")
            val splash = GoalTrainingServerClient.create().getSplash(
                Locale.getDefault().language,
                service,
                "${Build.BRAND} ${Build.MODEL}",
                if (time.contains(":")) time else "default",
                id
            )
            viewModelScope.launch {
                if (splash.isSuccessful) {
                    if (splash.body() != null) {
                        when (splash.body()!!.url) {
                            "no" -> downloadAssets()
                            "nopush" -> {
                                OneSignal.disablePush(true)
                                downloadAssets()
                            }
                            else -> viewModelScope.launch {
                                CurrentAppData.url = "https://${splash.body()!!.url}"
                                Navigator.navigateTo(Screens.WEB_VIEW)
                            }
                        }
                    } else downloadAssets()
                } else downloadAssets()
            }
        } catch (e: Exception) {
            downloadAssets()
        }
    }

    private fun downloadAssets() {
        assetTargets.clear()
        assetsToDownload = 0
        loadAsset("background", UrlBack)
        loadAsset("ball", UrlAsset_ball)
        loadAsset("gate", UrlAsset_gate)
        loadAsset("goalkeeper_idle", UrlAsset_goalkeeper_idle)
        loadAsset("goalkeeper_jump_high", UrlAsset_goalkeeper_jump_high)
        loadAsset("goalkeeper_jump_low", UrlAsset_goalkeeper_jump_low)
    }

    private fun loadAsset(name: String, url: String) {
        assetsToDownload++
        val target = object : Target {
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) { }
            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                switchToErrorScreen()
            }
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                if(bitmap != null) {
                    CurrentAppData.gameAssets.add(GameAsset(name, bitmap))
                    checkDownloads()
                } else {
                    switchToErrorScreen()
                }
            }
        }
        assetTargets.add(target)
        viewModelScope.launch {
            Picasso.get().load(url).into(target)
        }
    }

    private fun checkDownloads() {
        if(CurrentAppData.gameAssets.size >= assetsToDownload) switchToGame()
    }

    private fun switchToGame() {
        Navigator.navigateTo(Screens.GAME_SCREEN)
    }

    private fun switchToErrorScreen() {
        Navigator.navigateTo(Screens.ASSETS_LOADING_ERROR_SCREEN)
    }
}