package com.example.goaltraining.ui.screens.gameScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Dp
import com.example.goaltraining.R
import com.example.goaltraining.util.CurrentAppData

@Composable
fun Ball(state: Boolean?) {
    val ball = remember { CurrentAppData.getAsset("ball") }
    Box(modifier = Modifier.aspectRatio(1.0f)
        .fillMaxSize()
        .padding(Dp(5.0f)), contentAlignment = Alignment.Center) {
        if(state == null) {
            ball?.let {
                Image(it.bitmap.asImageBitmap(), contentDescription = "ball", alignment = Alignment.Center, contentScale = ContentScale.FillBounds, modifier = Modifier.fillMaxSize())
            }
        } else if(state) {
            Image(painterResource(id = R.drawable.ic_baseline_check_24), contentDescription = "success", alignment = Alignment.Center, contentScale = ContentScale.FillBounds, modifier = Modifier.fillMaxSize())
        } else Image(painterResource(id = R.drawable.ic_baseline_close_24), contentDescription = "failure", alignment = Alignment.Center, contentScale = ContentScale.FillBounds, modifier = Modifier.fillMaxSize())
    }
}