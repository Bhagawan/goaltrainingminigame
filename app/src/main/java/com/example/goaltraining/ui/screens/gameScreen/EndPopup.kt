package com.example.goaltraining.ui.screens.gameScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import com.example.goaltraining.R
import com.example.goaltraining.ui.theme.*

@Composable
fun EndPopup(result: List<Boolean?>, onClick : () -> Unit = {}) {
    Box(modifier = Modifier
        .fillMaxSize()
        .clickable { onClick() }
        .background(color = Grey_transparent), contentAlignment = Alignment.Center) {
        Column(
            modifier = Modifier
                .wrapContentSize()
                .padding(Dp(10.0f))
                .background(color = Grey, shape = RoundedCornerShape(Dp(20.0f)))
                .border(width = Dp(3.0f), color = Yellow, shape = RoundedCornerShape(Dp(20.0f)))
                .padding(Dp(10.0f)), horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(text = stringResource(id = R.string.end_header), fontSize = TextUnit(20.0f, TextUnitType.Sp), color = Color.White, textAlign = TextAlign.Center)
            Box(modifier = Modifier.sizeIn(maxWidth = Dp(300.0f), maxHeight = Dp(100.0f))) { ResultRow(result = result) }
            Image(painter = painterResource(id = R.drawable.ic_baseline_reload), contentDescription = stringResource(
                id = R.string.desc_reset), Modifier.background(Grey_light, Shapes.medium))
        }
    }
}