package com.example.goaltraining.ui.screens.gameScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.goaltraining.ui.theme.Grey
import com.example.goaltraining.ui.theme.Grey_light
import com.example.goaltraining.util.CurrentAppData
import com.example.goaltraining.view.GameView
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Preview
@Composable
fun GameScreen() {
    val viewModel = viewModel<GameViewModel>()
    val background = CurrentAppData.getAsset("background")
    val result: List<Boolean?> by viewModel.resultFlow.collectAsState()
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        background?.let {
            Image(it.bitmap.asImageBitmap(), contentDescription = "field", modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
        }
        AndroidView(factory = { GameView(it) },
            modifier = Modifier.fillMaxSize(),
            update = { view ->
                viewModel.restartFlow.onEach { if(it) view.restart() }.launchIn(viewModel.viewModelScope)
                viewModel.endPopup.onEach { if(it) view.stop() }.launchIn(viewModel.viewModelScope)
                view.setInterface(object: GameView.GoalTrainingInterface {
                    override fun onHit(goal: Boolean) {
                        viewModel.hit(goal)
                    }
                })
            })
        Box(modifier = Modifier
            .sizeIn(maxHeight = Dp(60.0f), maxWidth = Dp(300.0f))
            .wrapContentSize()
            .align(Alignment.TopCenter)
            .padding(Dp(5.0f))
            .background(color = Grey_light, shape = RoundedCornerShape(Dp(10.0f)))
            .border(width = Dp(2.0f), color = Grey, shape = RoundedCornerShape(Dp(10.0f)))) {
            ResultRow(result = result)
        }
    }

    val introPopup by viewModel.introPopup.collectAsState(true)
    val endPopup by viewModel.endPopup.collectAsState(false)

    if(introPopup) IntroPopup(viewModel::restart)
    else if(endPopup) EndPopup(result, viewModel::restart)
}