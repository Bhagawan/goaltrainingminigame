package com.example.goaltraining.ui.screens.gameScreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class GameViewModel: ViewModel() {
    private val _introPopup = MutableStateFlow(true)
    val introPopup = _introPopup.asStateFlow()

    private val _endPopup = MutableStateFlow(false)
    val endPopup = _endPopup.asStateFlow()

    private val _restartFlow = MutableSharedFlow<Boolean>()
    val restartFlow = _restartFlow.asSharedFlow()

    private var result = arrayListOf<Boolean?> (null, null, null, null, null)

    private val _resultFlow = MutableStateFlow(result.toList())
    val resultFlow = _resultFlow.asStateFlow()


    fun restart() {
        _introPopup.tryEmit(false)
        _endPopup.tryEmit(false)
        result = arrayListOf(null, null, null, null, null)
        _resultFlow.tryEmit(result.toList())
        viewModelScope.launch {
            _restartFlow.emit(true)
        }
    }

    fun hit(goal: Boolean) {
        for(n in result.indices) if(result[n] == null || n == result.size - 1) {
            result[n] = goal
            break
        }
        val r = ArrayList<Boolean?>()
        for(n in result.indices) r.add(result[n])
        _resultFlow.tryEmit(r)
        if(result.last() != null) {
            _endPopup.tryEmit(true)
        }
    }
}