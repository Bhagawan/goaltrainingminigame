package com.example.goaltraining.ui.screens.gameScreen

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import com.example.goaltraining.R
import com.example.goaltraining.ui.theme.Grey
import com.example.goaltraining.ui.theme.Grey_transparent
import com.example.goaltraining.ui.theme.Yellow

@Composable
fun IntroPopup(onClick : () -> Unit = {}) {
    Box(modifier = Modifier
        .fillMaxSize()
        .clickable { onClick() }
        .background(color = Grey_transparent), contentAlignment = Alignment.Center) {
        Box(
            modifier = Modifier
                .sizeIn(maxWidth = Dp(200.0f))
                .wrapContentSize()
                .background(color = Grey, shape = RoundedCornerShape(Dp(20.0f)))
                .border(width = Dp(3.0f), color = Yellow, shape = RoundedCornerShape(Dp(20.0f)))
                .padding(Dp(20.0f))
        ) {
            Text(text = stringResource(id = R.string.intro), fontSize = TextUnit(20.0f, TextUnitType.Sp), color = Color.White, textAlign = TextAlign.Center)
        }
    }
}