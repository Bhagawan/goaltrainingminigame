package com.example.goaltraining.ui.screens.gameScreen

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp

@Composable
fun ResultRow(result: List<Boolean?>) {
    Row(modifier = Modifier
        .fillMaxSize()
        .padding(Dp(10.0f))) {
        for(r in result) {
            Box(modifier = Modifier.weight(1.0f, true)) {
                Ball(r)
            }
        }
    }
}