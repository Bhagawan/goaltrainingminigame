package com.example.goaltraining.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val Orange = Color(0xFF94512C)
val Grey_light = Color(0xFF8B7F7F)
val Grey = Color(0xFF494444)
val Red = Color(0xFFA72E2E)
val Yellow = Color(0xFF9E6E29)
val Grey_transparent = Color(0x80000000)