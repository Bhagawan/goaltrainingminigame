package com.example.goaltraining.util

object CurrentAppData {
    var url = ""

    val gameAssets = ArrayList<GameAsset>()

    fun getAsset(name: String): GameAsset? {
        for(asset in gameAssets) if(asset.name == name) return asset
        return null
    }
}