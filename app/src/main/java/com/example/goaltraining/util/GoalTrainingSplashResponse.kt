package com.example.goaltraining.util

import androidx.annotation.Keep

@Keep
data class GoalTrainingSplashResponse(val url : String)