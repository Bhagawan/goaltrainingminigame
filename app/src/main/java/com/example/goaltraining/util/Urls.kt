package com.example.goaltraining.util

const val UrlSplash = "GoalTrainingMiniGame/splash.php"
const val UrlBack = "http://195.201.125.8/GoalTrainingMiniGame/back.png"
const val UrlAsset_ball = "http://195.201.125.8/GoalTrainingMiniGame/ball.png"
const val UrlAsset_gate = "http://195.201.125.8/GoalTrainingMiniGame/gate.png"
const val UrlAsset_goalkeeper_idle = "http://195.201.125.8/GoalTrainingMiniGame/goalkeeper_idle.png"
const val UrlAsset_goalkeeper_jump_high = "http://195.201.125.8/GoalTrainingMiniGame/goalkeeper_jump_high.png"
const val UrlAsset_goalkeeper_jump_low = "http://195.201.125.8/GoalTrainingMiniGame/goalkeeper_jump_low.png"