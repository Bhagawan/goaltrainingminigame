package com.example.goaltraining.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.drawable.toBitmap
import com.example.goaltraining.R
import com.example.goaltraining.util.CurrentAppData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.sign
import kotlin.random.Random

class GameView(context: Context): View(context){
    private var mWidth = 0
    private var mHeight = 0
    private var bottomOff = 0.0f
    private var ballSize = 1.0f
    private var ballSpeed = 1.0f

    private var ballX = 0.0f
    private var ballY = 0.0f

    companion object {
        const val STATE_ANIMATION = 0
        const val STATE_HIT_POPUP = 3
        const val STATE_AIM = 1
        const val STATE_PAUSE = 2
    }
    private val targetBitmap = AppCompatResources.getDrawable(context, R.drawable.ic_aim)?.toBitmap()
    private var goalkeeperIdle: Bitmap? = null
    private var goalkeeperJumpLowLeft: Bitmap? = null
    private var goalkeeperJumpHighLeft: Bitmap? = null
    private var goalkeeperJumpLowRight: Bitmap? = null
    private var goalkeeperJumpHighRight: Bitmap? = null
    private val ball = CurrentAppData.getAsset("ball")
    private val gate = CurrentAppData.getAsset("gate")
    private val back = CurrentAppData.getAsset("back")

    private var goalkeeperPose = 0 //0 - idle, 1 - left Bottom, 2 - leftTop, 3 rightTop, 4 - right Bottom
    private var hitBallTo = 1
    private var goal = false
    private var hitPopupTextSize = 1.0f

    private var state = STATE_PAUSE

    private var mInterface: GoalTrainingInterface? = null

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            bottomOff = mHeight * 0.1f
            ballSize = mHeight * 0.2f
            createGoalkeeper()
            ballX = mWidth / 2.0f
            ballY = mHeight - ballSize / 2.0f
            ballSpeed = 20.0f
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            drawField(it)
            drawGoalkeeper(it)
            if(state == STATE_AIM) drawTargets(it)
            if(state == STATE_ANIMATION) updateBall()
            drawBall(it)
            if(state == STATE_HIT_POPUP) {
                updatePopup()
                drawPopup(it)
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                return true
            }
            MotionEvent.ACTION_UP -> {
                when(state) {
                    STATE_AIM -> checkTouch(event.x, event.y)
                }
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    /// Public

    fun setInterface(i: GoalTrainingInterface) {
        mInterface = i
    }

    fun restart() {
        resetBall()
    }

    fun stop() {
        state = STATE_PAUSE
    }

    //// Private

    private fun drawField(c: Canvas) {
        val p = Paint()
        back?.let {
            c.drawBitmap(it.bitmap, null,Rect(0,0, mWidth, mHeight), p)
        }
        gate?.let {
            c.drawBitmap(it.bitmap, null,Rect(0,0, mWidth, mHeight - bottomOff.toInt()), p)
        }
    }

    private fun drawGoalkeeper(c: Canvas) {
        val topPadding = mHeight * 0.1f
        val p = Paint()
        val hPad = mWidth * 0.05f
        when(goalkeeperPose) {
            1 -> goalkeeperJumpLowLeft?.let { c.drawBitmap(it, hPad, mHeight - bottomOff - it.height, p) }
            2 -> goalkeeperJumpHighLeft?.let { c.drawBitmap(it, hPad, topPadding, p) }
            3 -> goalkeeperJumpHighRight?.let { c.drawBitmap(it, mWidth - hPad - it.width, topPadding, p) }
            4 -> goalkeeperJumpLowRight?.let { c.drawBitmap(it, mWidth - hPad - it.width, mHeight - bottomOff - it.height, p) }
            else -> goalkeeperIdle?.let { c.drawBitmap(it, (mWidth - it.width) / 2.0f, mHeight - bottomOff - it.height, p) }
        }
    }

    private fun drawTargets(c: Canvas) {
        targetBitmap?.let {
            val p = Paint()
            val topPadding = mHeight * 0.1f
            val hPad = mWidth * 0.05f
            val radius = ballSize / 2
            p.color = Color.parseColor("#90000000")
            c.drawCircle(hPad + radius, radius + topPadding, radius * 1.2f, p)
            c.drawBitmap(it, null, Rect(hPad.toInt(), topPadding.toInt(), (hPad + ballSize).toInt(), (topPadding + ballSize).toInt()), p)
            c.drawCircle(hPad + radius, mHeight - bottomOff - radius, radius * 1.2f, p)
            c.drawBitmap(it, null, Rect(hPad.toInt(), (mHeight - bottomOff - ballSize).toInt(), (hPad + ballSize).toInt(), (mHeight - topPadding).toInt()), p)
            c.drawCircle(mWidth - hPad - radius, topPadding + radius, radius * 1.2f, p)
            c.drawBitmap(it, null, Rect((mWidth - ballSize - hPad).toInt(), topPadding.toInt(), (mWidth - hPad).toInt(), (topPadding + ballSize).toInt()), p)
            c.drawCircle(mWidth - hPad - radius, mHeight - bottomOff - radius, radius * 1.2f, p)
            c.drawBitmap(it, null, Rect((mWidth - ballSize - hPad).toInt(), (mHeight - bottomOff - ballSize).toInt(), (mWidth - hPad).toInt(), (mHeight - topPadding).toInt()), p)
        }
    }

    private fun updateBall() {
        if (state == STATE_ANIMATION) {
            val topPadding = mHeight * 0.1f
            val hPad = mWidth * 0.05f
            val radius = ballSize / 2
            val targetX: Float
            val targetY: Float
            when(hitBallTo) {
                1 -> {
                    targetX = hPad + radius
                    targetY = mHeight - bottomOff - radius
                }
                2 -> {
                    targetX = hPad + radius
                    targetY = topPadding + radius
                }
                3 -> {
                    targetX = mWidth - hPad - radius
                    targetY = topPadding + radius
                }
                else -> {
                    targetX = mWidth - hPad - radius
                    targetY = mHeight - bottomOff - radius
                }
            }
            if(ballX == targetX && ballY == targetY) {
                jump()
            } else {
                ballX = (ballX + (targetX - mWidth / 2).sign * ballSpeed).coerceIn(hPad + radius, mWidth - hPad - radius)
                ballY = (ballY + (targetY - mHeight + radius + bottomOff).sign * ballSpeed).coerceIn(topPadding + radius, mHeight - bottomOff - radius)
            }
        }
    }

    private fun drawBall(c: Canvas) {
        ball?.let {
            c.drawBitmap(it.bitmap, null, Rect((ballX - ballSize / 2).toInt(), (ballY - ballSize / 2).toInt(), (ballX + ballSize / 2).toInt(), (ballY + ballSize / 2).toInt()), Paint())
        }
    }

    private fun jump() {
        goalkeeperPose = Random.nextInt(4) + 1
        goal = hitBallTo != goalkeeperPose
        state = STATE_HIT_POPUP
        mInterface?.onHit(goal)
    }

    private fun updatePopup() {
        hitPopupTextSize+=2
        if(hitPopupTextSize > 120) resetBall()
    }

    private fun drawPopup(c: Canvas) {
        val p = Paint()
        p.textAlign = Paint.Align.CENTER
        p.isFakeBoldText = true
        p.textSize = hitPopupTextSize
        p.color = if(goal) Color.YELLOW else Color.RED
        c.drawText(if(goal) "Гол" else "Неудача", mWidth / 2.0f, mHeight / 2.0f, p)
    }

    private fun checkTouch(x: Float, y: Float) {
        val topPadding = mHeight * 0.1f
        val hPad = mWidth * 0.05f
        if(x in hPad..hPad + ballSize) {
            if(y in topPadding..topPadding + ballSize) {
                hitBallTo = 2
                state = STATE_ANIMATION
            } else if(y in (mHeight - bottomOff - ballSize)..(mHeight - bottomOff)) {
                hitBallTo = 1
                state = STATE_ANIMATION
            }
        } else if(x in (mWidth - hPad - ballSize)..(mWidth - hPad)) {
            if(y in topPadding..topPadding + ballSize) {
                hitBallTo = 3
                state = STATE_ANIMATION
            } else if(y in (mHeight - bottomOff - ballSize)..(mHeight - bottomOff)) {
                hitBallTo = 4
                state = STATE_ANIMATION
            }
        }
    }

    private fun resetBall() {
        ballSize = mHeight * 0.1f
        ballX = mWidth / 2.0f
        ballY = mHeight - ballSize / 2.0f
        state = STATE_AIM
        goalkeeperPose = 0
        hitPopupTextSize = 0.0f
    }

    private fun createGoalkeeper() {
        val gIdle = CurrentAppData.getAsset("goalkeeper_idle")
        val gLowRight = CurrentAppData.getAsset("goalkeeper_jump_low")
        val gHighLeft = CurrentAppData.getAsset("goalkeeper_jump_high")
        gLowRight?.let {
            val matrix = Matrix()
            val dX = (mWidth / 3.0f) / it.bitmap.width.toFloat()
            matrix.postScale( -dX , dX, it.bitmap.width / 2f, it.bitmap.height / 2f)
            goalkeeperJumpLowLeft = Bitmap.createBitmap(it.bitmap, 0, 0, it.bitmap.width, it.bitmap.height, matrix, true)
            matrix.postScale( -1.0f , 1.0f, it.bitmap.width / 2f, it.bitmap.height / 2f)
            goalkeeperJumpLowRight = Bitmap.createBitmap(it.bitmap, 0, 0, it.bitmap.width, it.bitmap.height, matrix, true)
        }
        gHighLeft?.let {
            val matrix = Matrix()
            val dX = (mWidth / 3.0f) / it.bitmap.width.toFloat()
            matrix.postScale( -dX , dX, it.bitmap.width / 2f, it.bitmap.height / 2f)
            goalkeeperJumpHighRight = Bitmap.createBitmap(it.bitmap, 0, 0, it.bitmap.width, it.bitmap.height, matrix, true)
            matrix.postScale( -1.0f , 1.0f, it.bitmap.width / 2f, it.bitmap.height / 2f)
            goalkeeperJumpHighLeft = Bitmap.createBitmap(it.bitmap, 0, 0, it.bitmap.width, it.bitmap.height, matrix, true)
        }
        gIdle?.let {
            val matrix = Matrix()
            val dX = (mWidth / 7.0f) / it.bitmap.width
            matrix.postScale( dX, dX)
            goalkeeperIdle = Bitmap.createBitmap(it.bitmap, 0, 0, it.bitmap.width, it.bitmap.height, matrix, true)
        }
    }

    interface GoalTrainingInterface {
        fun onHit(goal: Boolean)
    }
}